# Contributing to Eclipse CHESS

This guide provides all necessary information to enable [contributors and committers](https://www.eclipse.org/projects/dev_process/#2_3_1_Contributors_and_Committers) to contribute to Eclipse CHESS. 

## Eclipse CHESS  
CHESS implements the CHESS UML/SysML profile, a specialization of the Modeling and Analysis of Real-Time and Embedded Systems (MARTE) profile, by producing extensions to Papyrus that provide component-based engineering methodology and tool support for the development of high-integrity embedded systems in different domains like satellite on board systems. 

## Developer resources

  * [CHESS Website](https://www.eclipse.org/chess/start.html)
  * [CHESS devel Website](https://projects.eclipse.org/projects/polarsys.chess)
  * [Forum](https://www.eclipse.org/forums/index.php/f/529/)
  * Mailing list: Join our [developer list](https://accounts.eclipse.org/mailing-list/chess-dev)
  * Bugs? [BugZilla](https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Chess) is where to report them

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team, contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit

## Contact

Contact the project developers via the project's "dev" list.

* chess-dev@eclipse.org

## How to contribute

The CHESS source code can be found [here](git://git.eclipse.org/gitroot/chess/chess.git).

We use Gerrit to review all changes by (non-committer) contributors before
they are merged:

https://git.eclipse.org/r/chess/chess

To build the project, go to repo\org.polarsys.chess.parent\ and execute mvn -P "Eclipse-Neon-Java8" install.

The branch 'devel' contains the contributions that will be included in the next release. 

### Committer contribution process
1. (you) If needed, open Issue on BugZilla 
2. (you) Create a branch from the devel branch. The branch prefix name is the ID of the issue. 
3. (you) Work on it
4. (you) Run regression tests
5. (other committer) Review the code 
6. (other committer) Merge new branch into devel
7. (other committer) Update CHESS_devel update-site and bundle
