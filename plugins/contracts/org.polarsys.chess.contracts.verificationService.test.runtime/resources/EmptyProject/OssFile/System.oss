
@requires discrete-time
COMPONENT System system
INTERFACE
INPUT speed : real ;
OUTPUT sensed_speed : real ;
OUTPUT sensed_speed_is_present : boolean ;
CONTRACT Sense
assume : speed=0 & G ( ( next ( speed ) - speed ) <=1
	and ( next ( speed ) - speed ) >=-1 ) ;
guarantee : /--we expect that: 
- there is always a sensed speed
- the delta between the speed and the sensed speed is <= 2 
--/
always ( ( sensed_speed - speed <= 2 ) and ( sensed_speed - speed >= - 2 ) and
	    sensed_speed_is_present ) ;
REFINEMENT
SUB sensor2 : SpeedSensor ;
SUB monitor1 : MonitorPresence ;
SUB selector : Selector ;
SUB monitor2 : MonitorPresence ;
SUB sensor1 : SpeedSensor ;
CONNECTION sensor2.speed := speed ;
CONNECTION sensor1.speed := speed ;
CONNECTION monitor2.input_is_present := sensor2.sensed_speed_is_present ;
CONNECTION monitor1.input_is_present := sensor1.sensed_speed_is_present ;
CONNECTION selector.input2 := sensor2.sensed_speed ;
CONNECTION selector.input1 := sensor1.sensed_speed ;
CONNECTION selector.input1_is_present := sensor1.sensed_speed_is_present ;
CONNECTION selector.input2_is_present := sensor2.sensed_speed_is_present ;
CONNECTION sensed_speed := selector.output ;
CONNECTION sensed_speed_is_present := selector.output_is_present ;
CONNECTION selector.switch_current_use := monitor1.absence_alarm or monitor2.absence_alarm ;
CONNECTION monitor1.enabled := ( selector.current_use=1 ) ;
CONNECTION monitor2.enabled := ( selector.current_use=2 ) ;
CONTRACT Sense REFINEDBY sensor1.Sense , sensor2.Sense , selector.Select , selector.Switch , monitor2.Monitor , monitor1.Monitor ;
COMPONENT SpeedSensor
INTERFACE
INPUT speed : real ;
OUTPUT sensed_speed : real ;
OUTPUT sensed_speed_is_present : boolean ;
CONTRACT Sense
assume : speed=0 & G ( ( next ( speed ) - speed ) <=1
	and ( next ( speed ) - speed ) >=-1 ) ;
guarantee : /--we expect that: 
- there is always a sensed speed
- the delta between the speed and the sensed speed is <= 1 
--/
always ( ( sensed_speed - speed <= 1 ) and ( sensed_speed - speed >= - 1 ) and
		      sensed_speed_is_present ) ;
COMPONENT MonitorPresence
INTERFACE
INPUT input_is_present : boolean ;
INPUT enabled : boolean ;
OUTPUT absence_alarm : boolean ;
CONTRACT Monitor
assume : input_is_present=TRUE ;
guarantee : /-- we expect that:
- an alarm is triggered whenever the monitor is enabled and the input is not present (is absent)
--/
always ( ( absence_alarm ) iff ( enabled and not ( input_is_present ) ) ) ;
COMPONENT Selector
INTERFACE
INPUT input1 : real ;
INPUT input1_is_present : boolean ;
INPUT input2 : real ;
INPUT input2_is_present : boolean ;
INPUT switch_current_use : boolean ;
OUTPUT current_use : 1 .. 2 ;
OUTPUT output : real ;
OUTPUT output_is_present : boolean ;
CONTRACT Select
assume : true ;
guarantee : ( output=0 and output_is_present = TRUE ) and always ( ( next ( current_use=1 ) implies ( next ( output ) =input1 and next ( output_is_present ) =input1_is_present ) ) and ( next ( current_use=2 ) implies ( next ( output ) =input2 and next ( output_is_present ) =input2_is_present ) ) ) ;
CONTRACT Switch
assume : true ;
guarantee : /--we expect that :
- the switch of the sensor depends only on the input boolean port 'switch_current_use'
--/
always ( ( ( current_use=1 and switch_current_use ) implies next ( current_use ) =2 ) and ( ( current_use=2 and switch_current_use ) implies next ( current_use ) =1 ) and ( ( not switch_current_use ) implies not change ( current_use ) ) ) ;