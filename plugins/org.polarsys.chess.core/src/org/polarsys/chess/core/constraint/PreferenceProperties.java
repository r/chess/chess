/*******************************************************************************
 * Copyright (C) 2020 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 ******************************************************************************/
package org.polarsys.chess.core.constraint;

/**
 * The Class PreferenceProperties.
 */
public class PreferenceProperties {
	
	/** The diagram in view. */
	public static String DIAGRAM_IN_VIEW="DiagramInView";
	
	/** The palettes in view. */
	public static String PALETTES_IN_VIEW="PaletteInView";
}
