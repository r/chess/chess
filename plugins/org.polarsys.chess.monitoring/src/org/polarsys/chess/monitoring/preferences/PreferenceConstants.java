/*
-----------------------------------------------------------------------
--                    CHESS monitoring plugin                        --
--                                                                   --
--                    Copyright (C) 2020                             --
--                     Intecs - Italy                                --
--                                                                   --                      --
--                                                                   --
-- All rights reserved. This program and the accompanying materials  --
-- are made available under the terms of the Eclipse Public License  --
-- v1.0 which accompanies this distribution, and is available at     --
-- http://www.eclipse.org/legal/epl-v20.html                         --
-----------------------------------------------------------------------
  */ 


package org.polarsys.chess.monitoring.preferences;

/**
 * Constant definitions for plug-in preferences.
 */
public class PreferenceConstants {

	/** The Constant COUNTER_SCALE_FACTOR_INT. */
	public static final String COUNTER_SCALE_FACTOR_INT = "Counter scale factor";
	
}
